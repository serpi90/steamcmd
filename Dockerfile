#Use the last LTS version of ubuntu
FROM ubuntu:20.04

LABEL maintainer="serpi90@gmail.com"

# Install dependencies and clean
# hadolint ignore=DL3008,DL3015
RUN set -eux; \
  dpkg --add-architecture i386; \
  apt-get update; \
  DEBIAN_FRONTEND=noninteractive apt-get install --assume-yes \
    ca-certificates \
    curl \
    lib32gcc1 \
    locales \
    ; \
 apt-get clean; \
 rm -rf /var/lib/apt/lists/*

# Instal utf8 locale to remove steamcmd warnings
RUN set -eux; \
 echo "en_US.UTF8 UTF-8" >> /etc/locale.gen; \
 locale-gen

RUN useradd \
  --shell /bin/bash \
  steam

#Switch to the user steam
# Download and extract steamcmd
WORKDIR /home/steam/steamcmd
RUN chown steam:steam /home/steam/steamcmd
USER steam

SHELL ["/bin/bash", "-c", "-o", "pipefail"]

RUN curl -fsSL http://media.steampowered.com/installer/steamcmd_linux.tar.gz | tar -vxz
RUN chmod u+x /home/steam/steamcmd/steamcmd.sh

VOLUME "/home/steam/steamcmd"